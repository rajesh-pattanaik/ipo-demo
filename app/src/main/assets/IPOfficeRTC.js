var callObj1=null, callObj2=null,callObj=null;
var callmap={};
var cli = new AWL.client();
$(document).ready(function() {
	  console.log("Document ready"+cli);
	  console.log(cli.isloggedIn());
	  var cfg = {
            serviceType: "phone",
            enableVideo: true,
            Gateway: {ip: "10.35.35.12"},
            Stunserver: {ip: "", port: "3478"},
            Turnserver: {ip: "", port: "3478", user: "", pwd: ""}
        };
      var videoCfg={
          localVideo : "remoteVideo",
          remoteVideo : "localVideo"
      }
	  var onCallListener = new CallListener();
	  cli.enableLogging();
	  if(cli.setConfiguration(cfg, onConfigChanged, onRegistrationStateChanged, onCallListener)){
          console.log("\nSetconfig success.");
      }
      if(cli.setDomElements(videoCfg)==="AWL_MSG_SETDOM_FAILED"){
            console.log("\nSETDOM FAILED");
      }else{
          console.log("\nSETDOM PASS");
      }
	  $('#mainPageDiv').hide();
	  console.log(cli.isloggedIn());
})
$("#answerCallBtn").click(
	function() {
		cli.answerCall(callObj1.getCallId());
        // var answerCallPopUp=document.getElementById("popup");
        //answerCallPopUp.classList.toggle("hide");
        $("#answerCallDiv").hide();
        $("#rejectCallDiv").hide();
	})
$("#rejectCallBtn").click(
	function (){
		cli.rejectCall(callObj1.getCallId());
        //var answerCallPopUp=document.getElementById("popup");
        //answerCallPopUp.classList.toggle("hide");
        $("#answerCallDiv").hide();
        $("#rejectCallDiv").hide();
	}
)
$("#dropIncomingCallBtn").click(
    function(){
        dropCall();
        var answerCallPopUp=document.getElementById("popup");
        answerCallPopUp.classList.toggle("show");
    }
)
$("#logOutBtn").on('click',function(){
        cli.logOut();
        $("#mainPageDiv").hide();
        $("#loginDiv").show();
})
        
$("#audioCallBtn").on('click',function(){
    makeCall("true");
})
$("#videoCallBtn").on('click',function(){
    makeCall("false");
})
$("#dropCallBtn").on('click',function(){
    dropCall();
})
$("#callTransferBtn").on('click',function(){
        var SIPExtn=$("#SIPExtn").val();
        console.log("The sip::"+ SIPExtn);
        if(callObj==null){
            alert("No ongoing call");
        }else{
            cli.transferCall(SIPExtn,callObj.getCallId(),"attended");
        }
});
$("#loginBtn").on('click',function(){
    var SIPExtn=$("#SIPExtn").val();
	var SIPPassword=$("#SIPPassword").val();
	console.log("Extension:: "+SIPExtn+ " SIP Password :: "+SIPPassword);
	cli.logIn(SIPExtn,SIPPassword, "true","false");
})
function makeCall(isAudio){
	number=$("#extn").val();
	console.log("The number dialled is ::" +number)
	if(isAudio==true){
		var objAudio=cli.makeCall(number,"audio");
	}
	else{
        console.log("The video call to::"+number);
		var objAudio=cli.makeCall(number,"video");
	}
	callObj=objAudio;
	console.log("The call Id" +objAudio.getCallId());
	console.log("Returns the current call state" +objAudio.getCallState());
	console.log("Returns the Far End number" +objAudio.getFarEndNumber());
	console.log("Returns the SipUri in string format" +objAudio.getSipUri());
}
function dropCall(){
	if(callObj==null){
        if(callObj1==null){
            alert("No ongoing call");
        }
		else{
            cli.dropCall(callObj1.getCallId());
            callObj1=null;
        }
	}else{
		cli.dropCall(callObj.getCallId());
		console.log("The call ended ,the id is :: "+callObj.getCallId());
		callObj=null;
	}
}
function onConfigChanged(){
	console.log("On config changes");
}
function onRegistrationStateChanged(resp){
	console.log("On registration changes"+resp);
    console.log('\n onRegistrationStateChange :: RESULT = ' + resp.result);
    console.log('\n onRegistrationStateChange :: reason = ' + resp.reason);
    if(resp.result === "AWL_MSG_LOGIN_SUCCESS") {
		$('#loginDiv').hide();
        $("#mainPageDiv").show();
	}else{
        alert("Invalid Credentials");
    }
}
function onCallListener(){
	console.log("On CallListener changes changes");	
}
var CallListener = function () {
    var _onNewIncomingCall = function (callId, callObj, autoAnswer) {
        console.log("onCallListener : onNewIncomingCall");
        console.log("onNewIncomingCall : getFarEndNumber = "+callObj.getFarEndNumber());
        console.log("onNewIncomingCall : getSipUri = "+callObj.getSipUri());
		console.log("onNewIncomingCall : autoAnswer = "+autoAnswer);
         var answerCallPopUp=document.getElementById("popup");
         answerCallPopUp.classList.toggle("show");
         $("#phoneAnswerDiv").show();
         $("#answerCallDiv").show();
         $("#rejectCallDiv").show();
        //answerCallPopUp.style.display = 'block';
        if (typeof(callmap[callId]) === 'undefined') {
            console.log("\n onCallStateChanged : New incoming CALL OBJECT ADDED");        
            if (callObj1 === null) {
                console.log("\n onCallStateChanged : CallObj assigned to callObj1");
                callObj1 = callObj;
                callmap[callObj1.getCallId()] = callObj1;
            }
            else if (callObj2 === null) {
                console.log("\n onCallStateChanged : CallObj assigned to callObj2");
                callObj2 = callObj;
                callmap[callObj2.getCallId()] = callObj;
            }
            else {
                console.log("\n onCallStateChanged : ALL LINES BUSY!!");
            }	
        }
    };
    var _onCallStateChange = function (callId, callObj, event) {
        console.log("\nSDK TEST: onCallStateChanged: ");
        console.log("\nSDK TEST: call Id " + callObj.getCallId());

        for (var key in callmap) {
            console.log("callMap[" + key + "]");
        }
        console.log("\nonCallStateChanged: Total Calls = " + Object.keys(callmap).length);
    };
    
    var _onCallTerminate = function(callObj, reason){
	      
    };
    return{
        onNewIncomingCall: _onNewIncomingCall,
        onCallStateChange: _onCallStateChange,
        onCallTerminate: _onCallTerminate
    };
};
